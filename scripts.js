

const isInViewport =(el)=> {
    const rect = el.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

const loop =()=>{
    const items = document.querySelectorAll('article')
    for(let item of items){
        if(isInViewport(item)){
            item.classList.remove('hidden')
        }
        else if(!item.classList.contains('hidden')){
            item.classList.add('hidden')
        }
    }
}
loop()
let limiter = new Date().getTime()
window.addEventListener('scroll', (ev) => {
    if((new Date().getTime() - limiter) < 50){
        return;
    }
    limiter = new Date().getTime()
    loop()
})